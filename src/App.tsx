import axios, { AxiosPromise } from 'axios';
import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

export const httpInstance = axios.create();
httpInstance.defaults.maxRedirects = 0;
httpInstance.interceptors.response.use(
  response => {
    console.log('>>> resp', response);
    return Promise.resolve(response)},
  error => {
    console.log('>>> error');
    if (error.response && [301, 302].includes(error.response.status)) {
      const redirectUrl = error.response.headers.location;
      return httpInstance.get(redirectUrl);
    }
    return Promise.reject(error);
  }
);

function App() {
  const [count, setCount] = useState(0)

  const dopost = () => {
    let url = "https://asmith.replaytrader.com/post.php";
    let payload = {foo:'bar'};

    httpInstance.post(url, payload).then((resp) => {
      console.log(resp);
      // let url = resp.data.redirectUrl;
      // if (resp.status === 200 && url) {
      //   window.location.href = url;
      // }
    });

  }

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <button onClick={() => dopost()}>Do Post</button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  )
}

export default App
